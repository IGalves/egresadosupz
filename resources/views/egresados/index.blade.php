@extends('egresados.template')

@section('encuesta')

<div class="container-fluid">
    <h4 class="c-grey-900 mT-10 mB-30">Encuestas contestadas</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Respuestas</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($egresados as $egresado)
                        <tr>
                            <th scope="row">{{$egresado->name}}</th>
                            <td>{{$egresado->email}}</td>
                            @php
                                $encuestas=App\Encuesta::where('user_id',$egresado->id)->get();
                                $forma=App\Forma::select("nombre")->get();
                            @endphp

                            <td>
                            @foreach($encuestas as $encuesta)
                            <a href="{{route('encuesta.show',$encuesta->id)}}">Respuestas de la encuesta {{$forma[$encuesta->forma-1]["nombre"]}}</a><br>
                            @endforeach
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    @endsection