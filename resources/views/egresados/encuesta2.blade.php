@extends('egresados.template')

@section('encuesta')
<h4 class="m-3">FORMATOS DEL ALUMNO AL CONCLUIR ESTADIAS (MA2019)</h4>
<small>Estimado Alumno estamos realizando cambios en las formas de evaluación, te pedimos de tu gran apoyo para hacer
    mas eficiente el proceso de conclusión de estadías. Por favor lee detenidamente lo que se solicita y ayudanos a
    mejorar contestando de manera objetiva cada una de las preguntas. Al final se te enviará una notificación al correo
que deberás imprimir junto con el acuse de las encuestas de la empresa y tu carta de liberación</small>
<div id="smartwizard" class="w-100">
    <ul>
        <li><a href="#step-1">Datos del Alumno</a></li>
        <li><a href="#step-2">Domicilio permanente</a></li>
        <li><a href="#step-3">Evaluación al programa educativo</a></li>
        <li><a href="#step-4">Evaluación del alumno a la empresa donde realizo estadías</a></li>
        <li><a href="#step-5">Cédula de egreso</a></li>
        <li><a href="#step-6">En caso de estar trabajando</a></li>
        <li><a href="#step-7">Datos de un familiar o amigo al que podamos contactar</a></li>
    </ul>

    <div class="m-4">
        <div id="step-1" class="">
            <form method="POST" action="{{route('encuesta.store','2')}}">
                @csrf
                <div class="form-group">
                    <label for="matricula">Matricula</label>
                    <input type="text" class="form-control" id="matricula" name="matricula">
                </div>

                <div class="form-group">
                    <label for="nombre">Nombre (s)</label>
                    <input type="text" class="form-control" id="nombre" name="nombre">
                </div>

                <div class="form-group">
                    <label for="apellido_paterno">Apellido Paterno</label>
                    <input type="text" class="form-control" id="apellido_paterno" name="apellido_paterno">
                </div>
                <div class="form-group">
                    <label for="apellido_materno">Apellido Materno</label>
                    <input type="text" class="form-control" id="apellido_materno" name="apellido_materno">
                </div>
                <div class="form-group">
                    <label for="fecha_nacimiento">Fecha de Nacimiento</label>
                    <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento">
                </div>

                <fieldset class="form-group">
                    <label for="genero">Género</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="genero" id="gridRadios1" value="hombre"
                                checked>
                                Hombre
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="genero" id="gridRadios2" value="mujer">
                                Mujer
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="estadoCivil">Estado civil</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadoCivil" id="gridRadios1"
                                value="soltero" checked>
                                Soltero
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadoCivil" id="gridRadios2"
                                value="casado">
                                Casado
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadoCivil" id="gridRadios2"
                                value="viudo">
                                Viudo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadoCivil" id="gridRadios2"
                                value="divorciado">
                                Divorciado
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadoCivil" id="gridRadios2"
                                value="unión libre">
                                Unión libre
                            </label>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div id="step-2" class="">
                <div class="form-group">
                    <label for="calle_numero">Calle y Número</label>
                    <input type="text" class="form-control" id="calle_numero" name="calle_numero">
                </div>

                <div class="form-group">
                    <label for="colonia">Colonia, Fraccionamiento, Barrio</label>
                    <input type="text" class="form-control" id="colonia" name="colonia">
                </div>

                <div class="form-group">
                    <label for="codigo_postal">Código Postal</label>
                    <input type="text" class="form-control" id="codigo_postal" name="codigo_postal">
                </div>

                <div class="form-group">
                    <label for="poblacion">Población, Comunidad o Localidad</label>
                    <input type="text" class="form-control" id="poblacion" name="poblacion">
                </div>

                <div class="form-group">
                    <label for="municipio">Municipio</label>
                    <input type="text" class="form-control" id="municipio" name="municipio">
                </div>

                <div class="form-group">
                    <label for="estado">Estado</label>
                    <input type="text" class="form-control" id="estado" name="estado">
                </div>

                <div class="form-group">
                    <label for="telefono">Teléfono particular (con lada)</label>
                    <input type="text" class="form-control" id="telefono" name="telefono">
                </div>
                <div class="form-group">
                    <label for="celular">Teléfono celular (con lada)</label>
                    <input type="text" class="form-control" id="celular" name="celular">
                </div>
                <div class="form-group">
                    <label for="email">Correo electrónico</label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>

                <fieldset class="form-group">
                    <label for="programaEducativo">Programa Educativo</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="programaEducativo" id="gridRadios1"
                                value="Licenciatura en Administración y Gestión de Pequeñas y Medianas Empresas"
                                checked>
                                Licenciatura en Administración y Gestión de Pequeñas y Medianas Empresas
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="programaEducativo" id="gridRadios2"
                                value="Licenciatura en Negocios Internacionales">
                                Licenciatura en Negocios Internacionales
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="programaEducativo" id="gridRadios3"
                                value="Ingeniería en Mecatrónica">
                                Ingeniería en Mecatrónica
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="programaEducativo" id="gridRadios4"
                                value="Ingeniería en Industrial">
                                Ingeniería en Industrial
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="programaEducativo" id="gridRadios5"
                                value="Ingeniería en Sistemas Computacionales">
                                Ingeniería en Sistemas Computacionales
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="programaEducativo" id="gridRadios6"
                                value="Ingeniería en Biotecnología">
                                Ingeniería en Biotecnología
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="programaEducativo" id="gridRadios7"
                                value="Ingeniería en Energía">
                                Ingeniería en Energía
                            </label>
                        </div>
                    </div>
                </fieldset>


                <fieldset class="form-group">
                    <label for="ingreso">Mes de ingreso</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ingreso" id="gridRadios1" value="Enero"
                                checked>
                                Enero
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ingreso" id="gridRadios2" value="Mayo">
                                Mayo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ingreso" id="gridRadios2"
                                value="Septiembre">
                                Septiembre
                            </label>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group">
                    <label for="aIngreso">Año de ingreso</label>
                    <input type="text" class="form-control" id="aIngreso" name="aIngreso">
                </div>

                <fieldset class="form-group">
                    <label for="egreso">Mes de egreso</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="egreso" id="gridRadios1" value="Abril"
                                checked>
                                Abril
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="egreso" id="gridRadios2" value="Agosto">
                                Agosto
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="egreso" id="gridRadios2"
                                value="Diciembre">
                                Diciembre
                            </label>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group">
                    <label for="aEgreso">Año de Egreso</label>
                    <input type="text" class="form-control" id="aEgreso" name="aEgreso">
                </div>

            </div>

            <div id="step-3" class="">
                <p>La valoración del programa educativo que estas estudiando es importante para la mejora en la calidad del
                    aprendizaje. Agradecemos tu colaboración para llenar los siguientes datos. Te recordamos que la
                    información es confidencial y sólo el área de Vinculación tiene acceso a los resultados de las encuestas
                </p>

                <fieldset class="form-group">
                    <label for="cursoPrope"> 1. En el curso propedeutico tuve una orientación respecto a mi
                    carrera</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cursoPrope" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cursoPrope" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cursoPrope" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cursoPrope" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cursoPrope" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>


                <fieldset class="form-group">
                    <label for="examenEgreso">2. Las materias impartidas durante mi carrera están relacionadas con el
                    examen Examen General de Egreso (EGEL)</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="examenEgreso" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="examenEgreso" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="examenEgreso" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="examenEgreso" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="examenEgreso" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>


                <fieldset class="form-group">
                    <label for="temasIngles">. 3. Los contenidos, temas y clases impartidas durante los 9
                        cuatrimestres de inglés fueron los idoneos para comunicarte fluidamente de manera oral y escrita en
                    este segundo idioma</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="temasIngles" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="temasIngles" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="temasIngles" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="temasIngles" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="temasIngles" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="preparacionToefl">3a. Los contenidos, temas y clases impartidas en tus nueve cursos de
                    inglés fueron los idoneos en tu preparación para el examen toefl</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="preparacionToefl" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="preparacionToefl" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="preparacionToefl" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="preparacionToefl" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="preparacionToefl" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="ambitoReal"> 4. La preparación académica que recibí, considero que es importante en
                    el ámbito real</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambitoReal" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambitoReal" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambitoReal" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambitoReal" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambitoReal" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="estadiasProfesionales"> 4a. La preparación académica que recibí considero me preparó para
                    realizar mis estadías en el campo profesional real </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiasProfesionales" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiasProfesionales" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiasProfesionales" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiasProfesionales" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiasProfesionales" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="proyectosDesarrollados"> 5. Los proyectos que desarrolle durante mi formación universitaria
                    complementaron mi formación académica</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectosDesarrollados" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectosDesarrollados" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectosDesarrollados" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectosDesarrollados" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectosDesarrollados" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="docentesNivel">6. Los docentes que me impartieron clases mostraron el nivel de
                    conocimiento y dominio del tema necesario para impartir la catedra</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesNivel" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesNivel" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesNivel" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesNivel" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesNivel" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="docentesLaboratorios"> 7. Los docentes que me impartieron clases en laboratorios mostraron el
                        nivel de conocimiento y dominio en el manejo de los equipos que se encuentran en laboratorios y
                    talleres </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesLaboratorios" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesLaboratorios" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesLaboratorios" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesLaboratorios" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="docentesLaboratorios" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="informacionEstadias"> 8. Recibí oportunamente la información necesaria sobre el procedimiento
                    de estadías</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="informacionEstadias" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="informacionEstadias" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="informacionEstadias" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="informacionEstadias" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="informacionEstadias" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="colocacionEstadias"> 8a. Se me dio seguimiento puntual para mi colocación de estadías
                    </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="colocacionEstadias" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="colocacionEstadias" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="colocacionEstadias" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="colocacionEstadias" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="colocacionEstadias" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="vinculacion"> 8b. La atención que recibí por parte de Vinculación durante mi proceso
                        académico fué el adecuado respecto a resolución de dudas, colocación de estadías y apoyo de trámites
                    </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="vinculacion" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="vinculacion" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="vinculacion" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="vinculacion" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="vinculacion" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="estadiaLaboral">9. La estadía complementó mi preparación para el mercado laboral</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaLaboral" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaLaboral" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaLaboral" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaLaboral" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaLaboral" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="estadiaFormacion"> 9a. La empresa donde realice la estadía fue acorde a mi formación
                    académica</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaFormacion" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaFormacion" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaFormacion" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaFormacion" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="estadiaFormacion" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="instalacionesUniv">10a. Las instalaciones de la Universidad (aulas, laboratorios, centros
                    de computo, biblioteca, canchas, gimnasio, etc.) fueron los adecuados para mi formación </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="instalacionesUniv" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="instalacionesUniv" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="instalacionesUniv" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="instalacionesUniv" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="instalacionesUniv" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="climaUniv">10b. El clima de la universidad (capacidad, cumplimiento y respeto por
                        parte de los profesores, el respeto a los derechos y obligaciones y el comportamiento de las
                        autoridades y la transparencia en el uso de recursos) fue desde mi punto de vista el adecuado para
                    mi formación </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="climaUniv" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="climaUniv" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="climaUniv" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="climaUniv" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="climaUniv" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="tutor"> 10c. Durante mi formación, tuve un tutor que me dio seguimiento
                    puntual</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tutor" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tutor" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tutor" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tutor" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tutor" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="formacionAsesorias"> 10d. Durante mi formación, recibí asesorías académicas cuando tuve
                    alguna dificultad en alguna materia</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionAsesorias" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionAsesorias" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionAsesorias" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionAsesorias" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionAsesorias" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="actividadExtra">11. Durante mi formación realice alguna actividad extracurricular
                        (deportiva o cultural) que me permitió desarrollar habilidades, destrezas, actitudes y
                    aptitudes</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadExtra" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadExtra" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadExtra" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadExtra" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadExtra" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="viajes"> 12. Durante mi formación realice viajes/visitas que complementaron mi
                    formación</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="viajes" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="viajes" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="viajes" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="viajes" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="viajes" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="emprendimiento">13. Durante mi formación, realice alguna actividad de
                    Emprendimiento</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimiento" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimiento" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimiento" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimiento" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimiento" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="emprendimientoFormacion">14. Consideras que las actividades de Emprendimiento fomentadas por la
                    Universidad fueron desde mi punto de vista adecuadas para mi formación </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimientoFormacion" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimientoFormacion" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimientoFormacion" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimientoFormacion" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="emprendimientoFormacion" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="serviciosEscolares">15. El departamento de Servicios Escolares me atendió en tiempo y forma
                        en los trámites relacionados con mi trayectoria académica (inscripción, becas, reinscripción,
                    trámites de titulación) </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="serviciosEscolares" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="serviciosEscolares" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="serviciosEscolares" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="serviciosEscolares" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="serviciosEscolares" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="talleresLab">16. El servicio de los talleres y laboratorios (de la carrera y los de
                        computo) fuero los adecuados respecto a tener materiales, el software y la atención del personal de
                    apoyo</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="talleresLab" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="talleresLab" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="talleresLab" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="talleresLab" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="talleresLab" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="conectividad"> 18. Los servicios de Conectividad al interior de la universidad
                        (internet en aulas, laboratorios e inalambrica) fueron los adecuados durante mi formación académica
                    </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="conectividad" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="conectividad" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="conectividad" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="conectividad" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="conectividad" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="enfermeria">19. Los servicios de enfermería de la universidad fueron los adecuados
                    cuando los necesité y la atención fue suficiente y atenta </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="enfermeria" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="enfermeria" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="enfermeria" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="enfermeria" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="enfermeria" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="cafeteria">20. Los servicios de cafetería que se te ofrecieron en la universidad
                    fueron los adecuados</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cafeteria" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cafeteria" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cafeteria" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cafeteria" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cafeteria" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="director">21. El director de carrera durante mi formación académica me atendió
                    adecuadamente, me dio seguimiento y resolvió mis dudas en tiempo y forma </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="director" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="director" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="director" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="director" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="director" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="asistentes"> 22. El personal de apoyo (asistentes) de la Universidad durante mi
                    formación me atendieron de manera adecuada, atenta y resolvieron mis dudas </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asistentes" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asistentes" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asistentes" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asistentes" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asistentes" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="directivos">23. El personal directivo (rector, secretaria académica, secretaria
                        administrativa, subdirectores y jefes de departamento) considero realizan su trabajo de manera
                    adecuada y eficiente y cuando lo necesité me atendieron de forma atenta y respetuosa </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="directivos" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="directivos" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="directivos" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="directivos" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="directivos" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="formacionSatisfaccion">24. Considero que la formación que recibí de la Universidad es de mi
                    entera satisfacción</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionSatisfaccion" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionSatisfaccion" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionSatisfaccion" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionSatisfaccion" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="formacionSatisfaccion" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="satisfechoEstudios">25. Me siento satisfecho con el plan de estudios que curse durante mi
                    estancia en la Universidad </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="satisfechoEstudios" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="satisfechoEstudios" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="satisfechoEstudios" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="satisfechoEstudios" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="satisfechoEstudios" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group">
                    <label for="sugerencia">Agradecemos alguna otra valiosa sugerencia para mejorar el plan de
                    estudios de tu carrera.</label>
                    <input type="text" class="form-control" id="sugerencia" name="sugerencia">
                </div>

            </div>

            <div id="step-4" class="">
                <p>Con las respuesta de esta encuesta nos permites conocer tus opiniones acerca de la estadía que
                presentaste y saber si tu experiencia cumplió con tus expectativas</p>
                <div class="form-group">
                    <label for="empresaEstadia">55. Nombre de la Empresa donde realizaste la estadía </label>
                    <input type="text" class="form-control" id="empresaEstadia" name="empresaEstadia">
                </div>
                <div class="form-group">
                    <label for="asesorEmpresarial">56. Nombre Completo del Asesor Empresarial </label>
                    <input type="text" class="form-control" id="asesorEmpresarial" name="asesorEmpresarial">
                </div>

                <div class="form-group">
                    <label for="cargoAsesor">57. Cargo del Asesor Empresarial </label>
                    <input type="text" class="form-control" id="cargoAsesor" name="cargoAsesor">
                </div>
                <div class="form-group">
                    <label for="nombreProyecto">58. Nombre del Proyecto que desarrollaste en Estadías </label>
                    <input type="text" class="form-control" id="nombreProyecto" name="nombreProyecto">
                </div>
                <div class="form-group">
                    <label for="telefonoEmpresa">59. Teléfono de Contacto de la Empresa </label>
                    <input type="text" class="form-control" id="telefonoEmpresa" name="telefonoEmpresa">
                </div>
                <div class="form-group">
                    <label for="correoEmpresa">60. Correo de Contacto de la Empresa </label>
                    <input type="text" class="form-control" id="correoEmpresa" name="correoEmpresa">
                </div>

                <div class="form-group">
                    <label for="maestroEstadias">61. Nombre del maestro que dio seguimiento a tus estadías </label>
                    <input type="text" class="form-control" id="maestroEstadias" name="maestroEstadias">
                </div>

                <fieldset class="form-group">
                    <label for="trabajoValorado">1. El trabajo que desempeñé fue valorado por la empresa </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="hablidades"> 2. El empresario permitió la aplicación de mis conocimientos,
                    habilidades y destrezas </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="hablidades" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="hablidades" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="hablidades" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="hablidades" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="hablidades" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="confianza"> 3. Sentí confianza para realizar actividades </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="confianza" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="confianza" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="confianza" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="confianza" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="confianza" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="experiencia">. 4. La estadía me dio experiencia para mi futura vida
                    profesional</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="experiencia" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="experiencia" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="experiencia" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="experiencia" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="experiencia" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="importancia"> 5. Me delegaron actividades y responsabilidades importantes </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="importancia" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="importancia" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="importancia" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="importancia" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="importancia" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="explicacionAct"> 6. Me explicaron como realizar las actividades </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="explicacionAct" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="explicacionAct" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="explicacionAct" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="explicacionAct" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="explicacionAct" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="herramientas"> 7. Me facilitaron las herramientas necesarias para la realización de
                    las tareas asignadas</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="herramientas" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="herramientas" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="herramientas" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="herramientas" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="herramientas" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="comunicacion"> 8. Me comunicaban de las situaciones que se presentaban</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="comunicacion" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="comunicacion" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="comunicacion" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="comunicacion" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="comunicacion" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="aportaciones">9. Tomaron en cuenta mis aportaciones de mejora </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="aportaciones" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="aportaciones" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="aportaciones" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="aportaciones" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="aportaciones" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="ambienteAgradable"> 10.El ambiente de trabajo fue agradable </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambienteAgradable" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambienteAgradable" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambienteAgradable" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambienteAgradable" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="ambienteAgradable" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="cargaTrabajo"> 11. No hubo sobrecarga de trabajo por parte de mi jefe
                    inmediato</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cargaTrabajo" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cargaTrabajo" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cargaTrabajo" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cargaTrabajo" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="cargaTrabajo" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="horario"> 12. Respetaron mi horario de actividades </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="horario" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="horario" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="horario" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="horario" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="horario" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="empleoEmpresa"> 13. Tengo la oportunidad de empleo en la empresa </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="empleoEmpresa" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="empleoEmpresa" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="empleoEmpresa" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="empleoEmpresa" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="empleoEmpresa" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="tomaDecisiones"> 14. La empresa confío en mi capacidad para tomar decisiones</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tomaDecisiones" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tomaDecisiones" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tomaDecisiones" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tomaDecisiones" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tomaDecisiones" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="actividadesPerfil">15. Las actividades encomendadas fueron de acuerdo a mi perfil </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadesPerfil" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadesPerfil" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadesPerfil" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadesPerfil" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadesPerfil" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="documentacion"> 16. No tuve contratiempos para que me entregaran la documentación
                    requerida (carta de aceptación, carta de liberación y evaluación al asesor) </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="documentacion" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="documentacion" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="documentacion" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="documentacion" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="documentacion" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="asesor"> 17. Conté con un asesor empresarial que aclaró mis dudas e
                    inquietudes</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asesor" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asesor" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asesor" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asesor" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="asesor" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="tratoJefe"> 18. El trato que recibí fue adecuado por parte de mi jefe inmediato
                    </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="recomendarEstadia"> 19. Recomendaría a la empresa para futuros alumnos de estadía </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="recomendarEstadia" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="recomendarEstadia" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="recomendarEstadia" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="recomendarEstadia" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="recomendarEstadia" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="proyecto">20. Me proporcionaron la información requerida para realizar mi
                    proyecto</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyecto" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyecto" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyecto" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyecto" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyecto" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="seguimiento"> 21. Mi asesor de la Universidad me dio seguimiento puntual en el
                    desarrollo de mis estadías</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="seguimiento" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="seguimiento" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="seguimiento" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="seguimiento" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="seguimiento" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group">
                    <label for="proyectoEmpresa"> 22. Mi asesor de la Universidad resolvió mis dudas para realizar el
                    proyecto en la Empresa</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectoEmpresa" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                                Completamente de acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectoEmpresa" id="gridRadios2"
                                value="De acuerdo">
                                De acuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectoEmpresa" id="gridRadios2"
                                value="Indeciso">
                                Indeciso
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectoEmpresa" id="gridRadios2"
                                value="En desacuerdo">
                                En desacuerdo
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="proyectoEmpresa" id="gridRadios2"
                                value="Completamente en desacuerdo">
                                Completamente en desacuerdo
                            </label>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div id="step-5" class="">
                <p>Estas a punto de Egresar de nuestra Universidad y pasar a ser un Orgulloso Egresado UPZ y es para
                    nosotros de gran importancia seguir en contacto contigo. Ayúdanos a hacerlo proporcionándonos la
                información que se solicita. ¡Muchas gracias, y éxito en esta etapa que vas iniciar!</p>
                <fieldset class="form-group">
                    <label for="actividadesConcluir">Que actividades vas a realizar al concluir la Universidad</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadesConcluir" id="gridRadios1"
                                value="Estudiar" checked>
                                Estudiar
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadesConcluir" id="gridRadios2"
                                value="Estudiar y trabajar">
                                Estudiar y trabajar
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadesConcluir" id="gridRadios2"
                                value="Trabajar">
                                Trabajar
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="actividadesConcluir" id="gridRadios2"
                                value="Ninguna">
                                Ninguna
                            </label>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group">
                    <label for="seguirEstudio">En caso de ser de tu interés seguir estudiando: Nombre de la institución
                    donde deseas estudiar</label>
                    <input type="text" class="form-control" id="seguirEstudio" name="seguirEstudio">
                </div>

                <div class="form-group">
                    <label for="posgrado">Nombre de la carrera o posgrado que deseas estudiar</label>
                    <input type="text" class="form-control" id="posgrado" name="posgrado">
                </div>
            </div>

            <div id="step-6" class="">
                <div class="form-group">
                    <label for="nombreEmpresaTrabajo">Nombre de la empresa donde trabajas</label>
                    <input type="text" class="form-control" id="nombreEmpresaTrabajo" name="nombreEmpresaTrabajo">
                </div>

                <fieldset class="form-group">
                    <label for="sector">Sector al que perteneces la empresa donde trabajas </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="sector" id="gridRadios1"
                                value="Industria" checked>
                                Industria
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="sector" id="gridRadios2"
                                value="Comercio">
                                Comercio
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="sector" id="gridRadios2"
                                value="Servicios">
                                Servicios
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="sector" id="gridRadios2"
                                value="Empresa Propia">
                                Empresa Propia
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="sector" id="gridRadios2"
                                value="Educación">
                                Educación
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="sector" id="gridRadios2"
                                value="Educación">
                                Educación
                            </label>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group">
                    <label for="puestoEmpresa"> ¿Qué puesto desarrollas en la empresa?</label>
                    <input type="text" class="form-control" id="puestoEmpresa" name="puestoEmpresa">
                </div>

                <div class="form-group">
                    <label for="domicilioEmpresa"> Domicilio de la empresa (calle y número)</label>
                    <input type="text" class="form-control" id="domicilioEmpresa" name="domicilioEmpresa">
                </div>

                <div class="form-group">
                    <label for="codigoEmpresa">Código postal de la empresa</label>
                    <input type="text" class="form-control" id="codigoEmpresa" name="codigoEmpresa">
                </div>

                <div class="form-group">
                    <label for="coloniaEmpresa">Colonia, fraccionamiento, barrio donde esta la empresa</label>
                    <input type="text" class="form-control" id="coloniaEmpresa" name="coloniaEmpresa">
                </div>

                <div class="form-group">
                    <label for="localidadEmpresa">Localidad de la empresa</label>
                    <input type="text" class="form-control" id="localidadEmpresa" name="localidadEmpresa">
                </div>

                <div class="form-group">
                    <label for="municipioEmpresa">Municipio de la empresa</label>
                    <input type="text" class="form-control" id="municipioEmpresa" name="municipioEmpresa">
                </div>

                <div class="form-group">
                    <label for="estadoEmpresa">Estado donde se ubica la empresa</label>
                    <input type="text" class="form-control" id="estadoEmpresa" name="estadoEmpresa">
                </div>

                <div class="form-group">
                    <label for="telefonoEmpresa">Teléfono con lada de la Empresa</label>
                    <input type="text" class="form-control" id="telefonoEmpresa" name="telefonoEmpresa">
                </div>

                <div class="form-group">
                    <label for="correoEmpresa">Correo electrónico de la Empresa</label>
                    <input type="text" class="form-control" id="correoEmpresa" name="correoEmpresa">
                </div>

                <fieldset class="form-group">
                    <label for="nivelIngresos">Ingreso mensual neto</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios1"
                                value="No remunerado" checked>
                                No remunerado
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="De $1 a $2,500">
                                De $1 a $2,500
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="De $2,501 a $ 4,000">
                                De $2,501 a $ 4,000
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="De $4,001 A $6,000">
                                De $4,001 A $6,000
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="De $6,001 a $8,000">
                                De $6,001 a $8,000
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="De $8,001 a $10,000">
                                De $8,001 a $10,000
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="De $10,001 a $12,000">
                                De $10,001 a $12,000
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="Mas de $12,001">
                                Mas de $12,001
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="trabajoEstadias"> El trabajo que tienes actualmente ¿lo obtuviste donde realizaste las
                    estadías? </label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="trabajoEstadias" id="gridRadios1"
                                value="si" checked>
                                Si
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="trabajoEstadias" id="gridRadios2"
                                value="no">
                                No
                            </label>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label for="trabajoCarrera">El trabajo que desarrollas esta relacionado con tu carrera</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="trabajoCarrera" id="gridRadios1"
                                value="si" checked>
                                Si
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="trabajoCarrera" id="gridRadios2"
                                value="no">
                                No
                            </label>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div id="step-7" class="">
                <div class="form-group">
                    <label for="nombreFamiliar">Nombre Completo</label>
                    <input type="text" class="form-control" id="nombreFamiliar" name="nombreFamiliar">
                </div>

                <div class="form-group">
                    <label for="domicilioFamiliar">Domicilio (calle y número)</label>
                    <input type="text" class="form-control" id="domicilioFamiliar" name="domicilioFamiliar">
                </div>

                <div class="form-group">
                    <label for="coloniaFamiliar">Colonia</label>
                    <input type="text" class="form-control" id="coloniaFamiliar" name="coloniaFamiliar">
                </div>

                <div class="form-group">
                    <label for="localidadFamiliar">Localidad</label>
                    <input type="text" class="form-control" id="localidadFamiliar" name="localidadFamiliar">
                </div>

                <div class="form-group">
                    <label for="entidadFamiliar">Entidad</label>
                    <input type="text" class="form-control" id="entidadFamiliar" name="entidadFamiliar">
                </div>

                <div class="form-group">
                    <label for="codigoPostalFamiliar">Código postal </label>
                    <input type="text" class="form-control" id="codigoPostalFamiliar" name="codigoPostalFamiliar">
                </div>

                <div class="form-group">
                    <label for="telefonoFamiliar">Teléfono particular (lada)</label>
                    <input type="text" class="form-control" id="telefonoFamiliar" name="telefonoFamiliar">
                </div>

                <div class="form-group">
                    <label for="celularFamiliar">Teléfono celular</label>
                    <input type="text" class="form-control" id="celularFamiliar" name="celularFamiliar">
                </div>

                <div class="form-group">
                    <label for="correoFamiliar">Correo electrónico</label>
                    <input type="text" class="form-control" id="correoFamiliar" name="correoFamiliar">
                </div>

                <div class="form-group">
                    <label for="parentescoFamiliar">¿Que parentesco tiene contigo?</label>
                    <input type="text" class="form-control" id="parentescoFamiliar" name="parentescoFamiliar">
                </div>

                <input class="btn btn-primary btn-lg" type="submit" value="Enviar">
            </form>
        </div>
    </div>
</div>
@endsection