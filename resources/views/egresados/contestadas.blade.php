@extends('egresados.template')

@section('encuesta')

<div class="container-fluid">
    <h4 class="c-grey-900 mT-10 mB-30">Encuestas contestadas</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">Encuesta</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Respuestas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Encuesta basica para egresados UPZ</th>
                            <td>05/12/2019</td>
                            <td><a href="{{ route('respuestas') }}">Respuestas de la encuesta</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    @endsection