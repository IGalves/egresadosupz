@extends('egresados.template')

@section('encuesta')

<div class="container-fluid">
    <h4 class="c-grey-900 mT-10 mB-30">Respuestas de la encuesta {{$forma->nombre}}.</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
            	@for($i=0;$i<count($respuestas);$i++)
                <h4>{{$preguntas[$i]}}</h4>
                <p>{{$respuestas[$i]}}</p>
                @endfor
            </div>
        </div>
    </div>
</div>
@endsection