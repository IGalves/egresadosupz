@extends('layouts/app')

@section('css')
<link rel="stylesheet" href="/css/smart_wizard.css">
<link rel="stylesheet" href="/css/datatables.min.css">
@endsection

@section('content')
@include('layouts.sidebar')
@include('layouts.navbar')

<main class='bgc-grey-100'>
    <div id='mainContent'>
        <div class="container-fluid">
            <div class="row">
                @yield('encuesta')
            </div>
        </div>
    </div>
</main>

@endsection

{{-- @section('javascript')
<script src="/js/jquery.smartWizard.js"></script>
<script src="/js/datatables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
      $('#smartwizard').smartWizard();
      $('#dataTable').DataTable();
  });

  $("#sidebar-toggle").on("click", function(){
    $(".app").toggleClass("is-collapsed");
  });

  $(".sidebar").hover(function() { 
    $("#header").toggleClass("sticky-top");
  });

  $(".nav-item, .dropdown").on("click", function(){
    $(this).toggleClass("open");
  });
</script>
@endsection --}}