@extends('egresados.template')

@section('encuesta')
<h4 class="m-3">Encuesta basica para egresados UPZ</h4>
<div id="smartwizard" class="w-100">
    <ul>
        <li><a href="#step-1">Preguntas 1</a></li>
        <li><a href="#step-2">Preguntas 2</a></li>
        <li><a href="#step-3">Preguntas 3</a></li>
        <li><a href="#step-4">Preguntas 4</a></li>
        <li><a href="#step-5">Preguntas 5</a></li>
        <li><a href="#step-6">Preguntas 6</a></li>
    </ul>

    <div class="m-4">
        <div id="step-1" class="">
            <form method="POST" action="{{route('encuesta.store',"1")}}">
            @csrf
            <fieldset class="form-group">
                <label for="genero">Género</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="genero" id="gridRadios1" value="hombre"
                                checked>
                            Hombre
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="genero" id="gridRadios2" value="mujer">
                            Mujer
                        </label>
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre">
            </div>
            <div class="form-group">
                <label for="apellido_paterno">Apellido Paterno</label>
                <input type="text" class="form-control" id="apellido_paterno" name="apellido_paterno">
            </div>
            <div class="form-group">
                <label for="apellido_materno">Apellido Materno</label>
                <input type="text" class="form-control" id="apellido_materno" name="apellido_materno">
            </div>
            <div class="form-group">
                <label for="fecha_nacimiento">Fecha de Nacimiento</label>
                <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento">
            </div>
            <div class="form-group">
                <label for="lugar_nacimiento">Lugar de Nacimiento</label>
                <input type="text" class="form-control" id="lugar_nacimiento" name="lugar_nacimiento">
            </div>

            <div class="form-group">
                <label for="domicilio">Domicilio Actual (Calle y Número)</label>
                <input type="text" class="form-control" id="domicilio" name="domicilio">
            </div>

            <div class="form-group">
                <label for="colonia">Colonia</label>
                <input type="text" class="form-control" id="colonia" name="colonia">
            </div>

            <div class="form-group">
                <label for="municipio">Municipio</label>
                <input type="text" class="form-control" id="municipio" name="municipio">
            </div>

            <div class="form-group">
                <label for="estado">Estado</label>
                <input type="text" class="form-control" id="estado" name="estado">
            </div>

        </div>
        <div id="step-2" class="">

            <div class="form-group">
                <label for="codigo_postal">Código Postal</label>
                <input type="text" class="form-control" id="codigo_postal" name="codigo_postal">
            </div>

            <div class="form-group">
                <label for="telefono">Teléfono</label>
                <input type="text" class="form-control" id="telefono" name="telefono">
            </div>
            <div class="form-group">
                <label for="celular">Celular</label>
                <input type="text" class="form-control" id="celular" name="celular">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <label for="estado_civil">Estado Civil</label>
                <input type="text" class="form-control" id="estado_civil" name="estado_civil">
            </div>
            <div class="form-group">
                <label for="contacto_redSocial">Contacto por las redes sociales</label>
                <input type="text" class="form-control" id="contacto_redSocial" name="contacto_redSocial">
            </div>
            <div class="form-group">
                <label for="nombreContacto">Nombre de un contacto</label>
                <input type="text" class="form-control" id="nombreContacto" name="nombreContacto">
            </div>
            <div class="form-group">
                <label for="telefonoContacto">Teléfono de un Contacto</label>
                <input type="text" class="form-control" id="telefonoContacto" name="telefonoContacto">
            </div>
            <div class="form-group">
                <label for="correoContacto">Correo del contacto</label>
                <input type="text" class="form-control" id="correoContacto" name="correoContacto">
            </div>
            <div class="form-group">
                <label for="parentesco">Parentesco</label>
                <input type="text" class="form-control" id="parentesco" name="parentesco">
            </div>

        </div>
        <div id="step-3" class="">
            <fieldset class="form-group">
                <label for="programaEgreso">Programa Educativo del que egreso</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="programaEgreso" id="gridRadios1"
                                value="Licenciatura en Administración y Gestión de Pequeñas y Medianas Empresas"
                                checked>
                            Licenciatura en Administración y Gestión de Pequeñas y Medianas Empresas
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="programaEgreso" id="gridRadios2"
                                value="Licenciatura en Negocios Internacionales">
                            Licenciatura en Negocios Internacionales
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="programaEgreso" id="gridRadios3"
                                value="Ingeniería en Mecatrónica">
                            Ingeniería en Mecatrónica
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="programaEgreso" id="gridRadios4"
                                value="Ingeniería en Industrial">
                            Ingeniería en Industrial
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="programaEgreso" id="gridRadios5"
                                value="Ingeniería en Sistemas Computacionales">
                            Ingeniería en Sistemas Computacionales
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="programaEgreso" id="gridRadios6"
                                value="Ingeniería en Biotecnología">
                            Ingeniería en Biotecnología
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="programaEgreso" id="gridRadios7"
                                value="Ingeniería en Energía">
                            Ingeniería en Energía
                        </label>
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <label for="aIngreso">Año de ingreso</label>
                <input type="text" class="form-control" id="aIngreso" name="aIngreso">
            </div>
            <div class="form-group">
                <label for="aEgreso">Año de Egreso</label>
                <input type="text" class="form-control" id="aEgreso" name="aEgreso">
            </div>
            <fieldset class="form-group">
                <label for="titulado">Titulado</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="titulado" id="gridRadios1" value="si"
                                checked>
                            Si
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="titulado" id="gridRadios2" value="no">
                            No
                        </label>
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <label for="aTitulacion">Año de Titulación</label>
                <input type="text" class="form-control" id="aTitulacion" name="aTitulacion">
            </div>
            <fieldset class="form-group">
                <label for="trabajando">Trabaja actualmente?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajando" id="gridRadios1" value="si"
                                checked>
                            Si
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajando" id="gridRadios2" value="no">
                            No
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="tiempoTrabajo">¿Cuanto tiempo trascurrió desde que egresó hasta su titulación?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoTrabajo" id="gridRadios1"
                                value="Inmediato al concluir la carrera" checked>
                            Inmediato al concluir la carrera
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoTrabajo" id="gridRadios2"
                                value="Un año">
                            Un año
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoTrabajo" id="gridRadios2"
                                value="Dos años">
                            Dos años
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoTrabajo" id="gridRadios2"
                                value="Más de dos años">
                            Más de dos años
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="apoyoTitulo">Si no se encuentra titulado, como lo apoyamos para que se titule</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="apoyoTitulo" id="gridRadios1"
                                value="Preparación EGEL" checked>
                            Preparación EGEL
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="apoyoTitulo" id="gridRadios2"
                                value="Preparación TOEFL">
                            Preparación TOEFL
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="apoyoTitulo" id="gridRadios2"
                                value="Otra">
                            Otra
                        </label>
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <label for="otroApoyo"> Si su respuesta fue Otra en la anterior, especifique</label>
                <input type="text" class="form-control" id="otroApoyo" name="otroApoyo">
            </div>

            <fieldset class="form-group">
                <label for="motivosNoTitulo"> Motivo(s) por los que no se ha titulado</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="motivosNoTitulo" id="gridRadios1"
                                value="No he aprobado Egel" checked>
                            No he aprobado Egel
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="motivosNoTitulo" id="gridRadios2"
                                value="No he aprobado TOEFL">
                            No he aprobado TOEFL
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="motivosNoTitulo" id="gridRadios2"
                                value="No se requiere en mi trabajo">
                            No se requiere en mi trabajo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="motivosNoTitulo" id="gridRadios2"
                                value="Cuestiones Personales">
                            Cuestiones Personales
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="motivosNoTitulo" id="gridRadios2"
                                value="Otra">
                            Otra
                        </label>
                    </div>
                </div>
            </fieldset>

        </div>
        <div id="step-4" class="">

            <div class="form-group">
                <label for="otroMotivo"> Si su respuesta fue otra en la anterior, especifique</label>
                <input type="text" class="form-control" id="otroMotivo" name="otroMotivo">
            </div>
            <fieldset class="form-group">
                <label for="posgrado">Estudios de Posgrado</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="posgrado" id="gridRadios1" value="si"
                                checked>
                            Si
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="posgrado" id="gridRadios2" value="no">
                            No
                        </label>
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <label for="institucion_posgrado">*En caso estudios de Posgrado: Nombre de la Institución</label>
                <input type="text" class="form-control" id="institucion_posgrado" name="institucion_posgrado">
            </div>
            <div class="form-group">
                <label for="nombre_posgrado">*En caso estudios de Posgrado: Nombre del Programa</label>
                <input type="text" class="form-control" id="nombre_posgrado" name="nombre_posgrado">
            </div>
            <fieldset class="form-group">
                <label for="gradoPosgrado">*En caso estudios de Posgrado: Grado Obtenido </label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="gradoPosgrado" id="gridRadios1"
                                value="Diplomado" checked>
                            Diplomado
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="gradoPosgrado" id="gridRadios2"
                                value="Especialidad">
                            Especialidad
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="gradoPosgrado" id="gridRadios2"
                                value="Maestría">
                            Maestría
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="gradoPosgrado" id="gridRadios2"
                                value="Doctorado">
                            Doctorado
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset class="form-group">
                <label for="becaPosgrado">*En caso estudios de Posgrado: ¿Estuvo o está becado en sus estudios de
                    posgrado? </label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="becaPosgrado" id="gridRadios1" value="si"
                                checked>
                            Si
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="becaPosgrado" id="gridRadios2"
                                value="no">
                            No
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="instBeca">*En caso estudios de Posgrado: ¿Quién le otorgó la beca de
                    posgrado?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="instBeca" id="gridRadios1"
                                value="El trabajo" checked>
                            El trabajo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="instBeca" id="gridRadios2"
                                value="Dependencia Gubernamental">
                            Dependencia Gubernamental
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="instBeca" id="gridRadios2"
                                value="Dependencia en el Extranjero">
                            Dependencia en el Extranjero
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="instBeca" id="gridRadios2"
                                value="Convenio UPZ">
                            Convenio UPZ
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="bolsaUPZ"> La Bolsa de la UPZ ¿le ayudo de alguna manera a enterarse de las
                    vacantes?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="bolsaUPZ" id="gridRadios1" value="si"
                                checked>
                            Si
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="bolsaUPZ" id="gridRadios2" value="no">
                            No
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="trabajaActual"> ¿Trabaja actualmente?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajaActual" id="gridRadios1"
                                value="si" checked>
                            Si
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajaActual" id="gridRadios2"
                                value="no">
                            No
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="tiempoEmpleo">¿Cuánto tiempo le llevó conseguir empleo, una vez que concluyó sus
                    estudios? </label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoEmpleo" id="gridRadios1"
                                value="Por estadía" checked>
                            Por estadía
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoEmpleo" id="gridRadios2"
                                value="3 meses">
                            3 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoEmpleo" id="gridRadios2"
                                value="6 meses">
                            6 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoEmpleo" id="gridRadios2"
                                value="1 año">
                            1 año
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoEmpleo" id="gridRadios2"
                                value="Más de un año">
                            Más de un año
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoEmpleo" id="gridRadios2"
                                value="Durante estudios">
                            Durante estudios
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoEmpleo" id="gridRadios2"
                                value="Al egresar">
                            Al egresar
                        </label>
                    </div>
                </div>
            </fieldset>
        </div>

        <div id="step-5" class="">
            <fieldset class="form-group">
                <label for="dificultadesEmpleo">¿Qué dificultades tuvo al momento de buscar trabajo?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="dificultadesEmpleo" id="gridRadios1"
                                value="Poca experiencia laboral" checked>
                            Poca experiencia laboral
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="dificultadesEmpleo" id="gridRadios2"
                                value="La carrera es poco conocida">
                            La carrera es poco conocida
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="dificultadesEmpleo" id="gridRadios2"
                                value="La oferta laboral es limitada para la carrera que estudié">
                            La oferta laboral es limitada para la carrera que estudié
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="dificultadesEmpleo" id="gridRadios2"
                                value="Mi situación personal lo dificultó">
                            Mi situación personal lo dificultó
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="dificultadesEmpleo" id="gridRadios2"
                                value="Las ofertas laborales eran pocas atractivas">
                            Las ofertas laborales eran pocas atractivas
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="dificultadesEmpleo" id="gridRadios2"
                                value="Otra">
                            Otra
                        </label>
                    </div>
                </div>
            </fieldset>


            <fieldset class="form-group">
                <label for="tiempoInsLaboral">¿Cuánto tiempo ha trabajado desde que se insertó al mercado
                    laboral?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoInsLaboral" id="gridRadios1"
                                value="de 1 a 6 meses" checked>
                            de 1 a 6 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoInsLaboral" id="gridRadios2"
                                value="de 7 a 12 meses">
                            de 7 a 12 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoInsLaboral" id="gridRadios2"
                                value="de 13 a 18 meses">
                            de 13 a 18 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoInsLaboral" id="gridRadios2"
                                value="de 19 a 24 meses">
                            de 19 a 24 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoInsLaboral" id="gridRadios2"
                                value="de 25 a 36 meses">
                            de 25 a 36 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tiempoInsLaboral" id="gridRadios2"
                                value="Más de 36 meses">
                            Más de 36 meses
                        </label>
                    </div>
                </div>
            </fieldset>



            <fieldset class="form-group">
                <label for="antiguedadTrabajo">Antigüedad en el puesto actual</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="antiguedadTrabajo" id="gridRadios1"
                                value="de 1 a 6 meses" checked>
                            de 1 a 6 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="antiguedadTrabajo" id="gridRadios2"
                                value="de 7 a 12 meses">
                            de 7 a 12 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="antiguedadTrabajo" id="gridRadios2"
                                value="de 13 a 18 meses">
                            de 13 a 18 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="antiguedadTrabajo" id="gridRadios2"
                                value="de 19 a 24 meses">
                            de 19 a 24 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="antiguedadTrabajo" id="gridRadios2"
                                value="de 25 a 36 meses">
                            de 25 a 36 meses
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="antiguedadTrabajo" id="gridRadios2"
                                value="Más de 36 meses">
                            Más de 36 meses
                        </label>
                    </div>
                </div>
            </fieldset>


            <fieldset class="form-group">
                <label for="nivelIngresos">Nivel de ingresos</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios1"
                                value="Hasta 4 mil pesos" checked>
                            Hasta 4 mil pesos
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="Hasta 6 mil pesos">
                            Hasta 6 mil pesos
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="Hasta 8 mil pesos">
                            Hasta 8 mil pesos
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="Hasta 10 mil pesos">
                            Hasta 10 mil pesos
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="Hasta 12 mil pesos">
                            Hasta 12 mil pesos
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelIngresos" id="gridRadios2"
                                value="Mas de 12 mil pesos">
                            Mas de 12 mil pesos
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="expectativas">¿El nivel de ingresos cubre sus expectativas como profesionista?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="expectativas" id="gridRadios1" value="si"
                                checked>
                            Si
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="expectativas" id="gridRadios2"
                                value="no">
                            No
                        </label>
                    </div>
                </div>
            </fieldset>


            <fieldset class="form-group">
                <label for="localidadEmpleo">Localidad de empleo</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="localidadEmpleo" id="gridRadios1"
                                value="Zona de influencia (Fresnillo)" checked>
                            Zona de influencia (Fresnillo)
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="localidadEmpleo" id="gridRadios2"
                                value="Interior de Estado">
                            Interior de Estado
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="localidadEmpleo" id="gridRadios2"
                                value="Fuera del Estado">
                            Fuera del Estado
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="localidadEmpleo" id="gridRadios2"
                                value="Extranjero">
                            Extranjero
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="tamOrganizacion">Tamaño de la Organización</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tamOrganizacion" id="gridRadios1"
                                value="Micro (1-10 empleados)" checked>
                            Micro (1-10 empleados)
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tamOrganizacion" id="gridRadios2"
                                value="Pequeña (11-50 empleados)">
                            Pequeña (11-50 empleados)
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tamOrganizacion" id="gridRadios2"
                                value="Mediana (51-250 empleados)">
                            Mediana (51-250 empleados)
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tamOrganizacion" id="gridRadios2"
                                value="Más de 251 empleados">
                            Más de 251 empleados
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="sectorLaboral">¿En que tipo de sector trabaja?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="sectorLaboral" id="gridRadios1"
                                value="Público" checked>
                            Público
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="sectorLaboral" id="gridRadios2"
                                value="Privado">
                            Privado
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="sectorLaboral" id="gridRadios2"
                                value="Educativo">
                            Educativo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="sectorLaboral" id="gridRadios2"
                                value="Profesional Independiente">
                            Profesional Independiente
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="instanciaTrabajo">En caso de sector público ¿en que tipo de instancia trabaja?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="instanciaTrabajo" id="gridRadios1"
                                value="Federal" checked>
                            Federal
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="instanciaTrabajo" id="gridRadios2"
                                value="Estatal">
                            Estatal
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="instanciaTrabajo" id="gridRadios2"
                                value="Municipal">
                            Municipal
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="instanciaTrabajo" id="gridRadios2"
                                value="Descentralizados">
                            Descentralizados
                        </label>
                    </div>
                </div>
            </fieldset>


            <fieldset class="form-group">
                <label for="tipoEmpresa">Si trabaja en sector privado ¿en que tipo de empresa trabaja?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tipoEmpresa" id="gridRadios1"
                                value="Industria" checked>
                            Industria
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tipoEmpresa" id="gridRadios2"
                                value="Comercio">
                            Comercio
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tipoEmpresa" id="gridRadios2"
                                value="Servicios">
                            Servicios
                        </label>
                    </div>
                </div>
            </fieldset>
        </div>

        <div id="step-6" class="">
            <fieldset class="form-group">
                <label for="nivelLaboral">Si trabaja en sector educativo ¿en que tipo de nivel labora?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelLaboral" id="gridRadios1"
                                value="Nivel Superior" checked>
                            Nivel Superior
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelLaboral" id="gridRadios2"
                                value="Nivel Medio Superior">
                            Nivel Medio Superior
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelLaboral" id="gridRadios2"
                                value="Nivel Medio">
                            Nivel Medio
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="nivelLaboral" id="gridRadios2"
                                value="Nivel Básico">
                            Nivel Básico
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="independiente">Si trabaja como Profesional Independiente ¿que tipo de relación
                    tiene?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="independiente" id="gridRadios1"
                                value="Empresa Propia" checked>
                            Empresa Propia
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="independiente" id="gridRadios2"
                                value="Asociado">
                            Asociado
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="independiente" id="gridRadios2"
                                value="Empleado">
                            Empleado
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="independiente" id="gridRadios2"
                                value="Otro">
                            Otro
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="puestoTrabajo">¿Qué nivel jerárquico tiene en su puesto de trabajo?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoTrabajo" id="gridRadios1"
                                value="Operario" checked>
                            Operario
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoTrabajo" id="gridRadios2"
                                value="Técnico General">
                            Técnico General
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoTrabajo" id="gridRadios2"
                                value="Técnico Especializado">
                            Técnico Especializado
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoTrabajo" id="gridRadios2"
                                value="Administrativo">
                            Administrativo
                        </label>
                    </div>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoTrabajo" id="gridRadios2"
                                value="Supervisor">
                            Supervisor
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoTrabajo" id="gridRadios2"
                                value="Gerente">
                            Gerente
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoTrabajo" id="gridRadios2"
                                value="Director">
                            Director
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoTrabajo" id="gridRadios2"
                                value="Autoempleo">
                            Autoempleo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoTrabajo" id="gridRadios2"
                                value="Otro">
                            Otro
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset class="form-group">
                <label for="trabajoFormacion">El trabajo que tiene ¿es acorde a su formación profesional?</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoFormacion" id="gridRadios1"
                                value="si" checked>
                            Si
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoFormacion" id="gridRadios2"
                                value="no">
                            No
                        </label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="puestoCarrera">1. El trabajo que tengo esta relacionado con la carrera que
                    estudie</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoCarrera" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoCarrera" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoCarrera" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoCarrera" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="puestoCarrera" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>

                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="desarrolloProfesional">2. Mi empleo actual permite mi desarrollo profesional</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desarrolloProfesional" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desarrolloProfesional" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desarrolloProfesional" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desarrolloProfesional" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desarrolloProfesional" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>

                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="ambienteLaboral">3. El ambiente de trabajo me parece agradable</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="ambienteLaboral" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="ambienteLaboral" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="ambienteLaboral" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="ambienteLaboral" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="ambienteLaboral" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>

                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="expectativasEconomicas">4. La empresa cubre mis expectativas económicas</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="expectativasEconomicas" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="expectativasEconomicas" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="expectativasEconomicas" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="expectativasEconomicas" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="expectativasEconomicas" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>

                </div>
            </fieldset>

            <fieldset class="form-group">
                <label for="crecimientoLaboral">5. Tengo oportunidad de crecimiento en la empresa</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="crecimientoLaboral" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="crecimientoLaboral" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="crecimientoLaboral" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="crecimientoLaboral" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="crecimientoLaboral" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>
                </div>
            </fieldset>


            <fieldset class="form-group">
                <label for="trabajoEstudios">6. No dejaría mi trabajo actual para buscar otro mas acorde con mis
                    estudios</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoEstudios" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoEstudios" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoEstudios" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoEstudios" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoEstudios" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>

                </div>
            </fieldset>


            <fieldset class="form-group">
                <label for="desempLaboral">7. Se me brinda la capacitación necesaria para desempeñar mi trabajo
                </label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desempLaboral" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desempLaboral" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desempLaboral" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desempLaboral" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="desempLaboral" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>

                </div>
            </fieldset>


            <fieldset class="form-group">
                <label for="tratoTrabajo">8. El trato con mis compañeros de trabajo es cordial</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoTrabajo" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoTrabajo" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoTrabajo" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoTrabajo" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoTrabajo" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>

                </div>
            </fieldset>


            <fieldset class="form-group">
                <label for="tratoJefe">9. Recibo el trato adecuado de parte de mi jefe inmediato</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="tratoJefe" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>

                </div>
            </fieldset>


            <fieldset class="form-group">
                <label for="trabajoValorado">10. El trabajo que desempeño es valorado por la empresa </label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios1"
                                value="Completamente de acuerdo" checked>
                            Completamente de acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios2"
                                value="De acuerdo">
                            De acuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios2"
                                value="Indeciso">
                            Indeciso
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios2"
                                value="En desacuerdo">
                            En desacuerdo
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="trabajoValorado" id="gridRadios2"
                                value="Completamente en desacuerdo">
                            Completamente en desacuerdo
                        </label>
                    </div>

                </div>
            </fieldset>

            <input class="btn btn-primary btn-lg" type="submit" value="Enviar">
            </form>
        </div>
    </div>
</div>
@endsection