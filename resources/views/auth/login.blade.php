@extends('layouts.app')

@section('content')
<div class="peers ai-s fxw-nw h-100vh">
    <div class="d-n@sm- peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv"
    style='background-image: url("/assets/bg.jpg")'>
</div>
<div class="col-12 col-md-4 peer pX-40 pY-80 h-100 bgc-white scrollable pos-r" style='min-width: 320px;'>
    <img class="img-fluid mx-auto d-block w-25" src="/assets/logo.png" alt="">
    <h4 class="fw-300 c-grey-900 mB-40">Iniciar sesión</h4>
    <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="">
     @csrf
     <div class="form-group">
        <label class="text-normal text-dark">Correo electrónico</label>
        <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="usuario@ejemplo.com" value="{{old('email')}}">
        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group">
        <label class="text-normal text-dark">Contraseña</label>
        <input type="password" name="password" class="form-control" placeholder="Password">
    </div>
    <div class="form-group">
        <div class="peers ai-c jc-sb fxw-nw">
            <div class="peer">
                <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                    <input type="checkbox" id="inputCall1" name="inputCheckboxesCall" class="peer">
                    <label for="inputCall1" class=" peers peer-greed js-sb ai-c">
                        <span class="peer peer-greed">No cerrar sesión</span>
                    </label>
                </div>
            </div>
            <div class="peer">
                <button class="btn btn-primary">Ingresar</button>
            </div>
        </div>
    </div>
    <div class="py-5">
        @if (Route::has('register'))
        <a class="btn btn-link d-block" href="{{ route('register') }}">Registrarse</a>
        @endif
        @if (Route::has('password.request'))
        <a class="btn btn-link d-block" href="{{ route('password.request') }}">¿Has olvidado tu contraseña?</a>
        @endif
    </div>
</form>
</div>
</div>
@endsection
