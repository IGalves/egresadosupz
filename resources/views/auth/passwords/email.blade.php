@extends('layouts.app')

@section('content')
<div class="peers ai-s fxw-nw h-100vh">
    <div class="d-n@sm- peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv"
    style='background-image: url("/assets/bg2.jpg")'>
</div>
<div class="col-12 col-md-4 peer pX-40 pY-80 h-100 bgc-white scrollable pos-r" style='min-width: 320px;'>
    <img class="img-fluid mx-auto d-block w-25" src="/assets/logo.png" alt="">
    <h4 class="fw-300 c-grey-900 mB-40">Restablecer contraseña</h4>
    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="form-group">
            <label class="text-normal text-dark">Correo electrónico</label>
            <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="usuario@ejemplo.com" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
        <div class="peer">
            <button class="btn btn-primary">Enviar enlace de restablecimiento</button>
        </div>
    </form>
</div>
</div>
@endsection
