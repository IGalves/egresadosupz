<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield("css")
</head>
<body class="app">
    <div id="app">        
        @yield('content')
    </div>
    <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
        <span>Copyright © 2019 <strong>Universidad Politécnica de Zacatecas</strong>. All Rights Reserved</span>
    </footer>
    <script src="/js/jquery.smartWizard.js"></script>
    <script src="/js/datatables.min.js"></script>
    @yield("javascript")
    <script type="text/javascript">
       $(document).ready(function(){
        $('#smartwizard').smartWizard();
        var table = $('#dataTable').DataTable({
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
    });

       $("#sidebar-toggle").on("click", function(){
        $(".app").toggleClass("is-collapsed");
    });

       $(".sidebar").hover(function() { 
        $("#header").toggleClass("sticky-top");
    });

       $(".nav-item, .dropdown").on("click", function(){
        $(this).toggleClass("open");
    });
</script>
</body>
</html>
