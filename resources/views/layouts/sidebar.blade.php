<div class="sidebar">
    <div class="sidebar-inner">
        <div class="sidebar-logo">
            <div class="peers ai-c fxw-nw">
                <div class="peer peer-greed">
                    <a class="sidebar-link td-n" href="/">
                        <div class="peers ai-c fxw-nw">
                            <div class="peer">
                                <div class="logo">
                                    <img class="img-fluid" src="/assets/logo.png" alt="">
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="peer">
                    <div class="mobile-toggle sidebar-toggle">
                        <a href="" class="td-n">
                            <i class="ti-arrow-circle-left"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <ul class="sidebar-menu scrollable pos-r">
            {{-- <li class="nav-item mT-30 actived">
                <a class="sidebar-link" href="index.html">
                    <span class="icon-holder">
                        <i class="c-blue-500 ti-home"></i>
                    </span>
                    <span class="title">Encuestas</span>
                </a>
            </li> --}}
            @if(Auth::user()->tipo==='Egresado')
            <li class="nav-item dropdown">
              <a class="dropdown-toggle" href="javascript:void(0);">
                <span class="icon-holder">
                  <i class="c-red-500 ti-files"></i>
              </span>
              <span class="title">Encuestas</span>
              <span class="arrow">
                  <i class="ti-angle-right"></i>
              </span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a class='sidebar-link' href="{{route('encuesta.create',"1")}}">Encuesta Basica</a>
          </li>
          <li>
              <a class='sidebar-link' href="{{route('encuesta.create',"2")}}">Encuesta Estadias</a>
          </li>
      </ul>
  </li>
  @endif

  @if(Auth::user()->tipo==='Administrador')
  <li class="nav-item">
      <a class='sidebar-link' href="{{route('egresados.index')}}">
        <span class="icon-holder">
          <i class="c-teal-500 ti-calendar"></i>
      </span>
      <span class="title">Egresados</span>
  </a>
</li>

<li class="nav-item">
    <a class="sidebar-link" href="charts.html">
        <span class="icon-holder">
            <i class="c-indigo-500 ti-bar-chart"></i>
        </span>
        <span class="title">Estadisticas</span>
    </a>
</li>
@endif

</ul>
</div>
</div>