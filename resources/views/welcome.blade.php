<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Universidad Politécnica de Zacatecas</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</head>

<body>
    <header id="header" class="fixed-top">
        <div class="container">

            <div class="logo float-left">
                <a href="#intro" class="scrollto"><img src="/assets/logo.png" class="img-fluid"></a>
            </div>

            <nav class="main-nav float-right d-none d-lg-block">
                <ul>
                    <li class="active"><a href="#intro">Inicio</a></li>
                    <li><a href="#about">Acerca de</a></li>
                    <li><a href="#services">Oferta educativa</a></li>
                    <li><a href="#testimonials">Alumnos</a></li>
                    <li><a href="#contact">Contacto</a></li>

                    @auth
                    <li><a href="{{ route('admin.home') }}">{{Auth::user()->name}}</a></li>
                    @else
                    <li><a href="{{ route('login') }}">Iniciar sesión</a></li>
                    @endauth

                </ul>
            </nav>

        </div>
    </header>

    <section id="intro" class="clearfix">
        <div class="container">

            <div class="intro-img">
                <img src="/assets/inicio.svg" class="img-fluid">
            </div>

            <div class="intro-info">
                <h2>Universidad Politécnica de Zacatecas<br><span>Creemos en tí,</span><br>crecemos contigo. . . </h2>
                <div>
                    <a href="#about" class="btn-get-started scrollto">Nosotros</a>
                    <a href="#services" class="btn-services scrollto">Oferta educativa</a>
                </div>
            </div>

        </div>
    </section>

    <main id="main">

        <section id="about">
            <div class="container">

                <header class="section-header">
                    <h3>Nosotros</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua.</p>
                </header>

                <div class="row about-container">

                    <div class="col-lg-6 content order-lg-1 order-2">
                        <div class="icon-box wow fadeInUp">
                            <div class="icon"><i class="fa fa-book"></i></div>
                            <h4 class="title"><a href="">Misión</a></h4>
                            <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero
                            tempore, cum soluta nobis est eligendi</p>
                        </div>

                        <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
                            <div class="icon"><i class="fa fa-eye"></i></div>
                            <h4 class="title"><a href="">Visión</a></h4>
                            <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                            officia deserunt mollit anim id est laborum</p>
                        </div>

                        <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
                            <div class="icon"><i class="fa fa-male"></i></div>
                            <h4 class="title"><a href="">Valores</a></h4>
                            <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                            aliquip ex ea commodo consequat tarad limino ata</p>
                        </div>

                    </div>

                    <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
                        <img src="/assets/banner-bg.jpg" class="img-fluid" alt="">
                    </div>
                </div>

            </div>
        </section>

        <section id="services" class="section-bg">
            <div class="container">

                <header class="section-header">
                    <h3>Oferta educativa</h3>
                    <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant
                    vituperatoribus.</p>
                </header>

                <div class="row">

                    <div class="row about-extra">
                        <div class="col-lg-6 wow fadeInUp">
                            <img src="/assets/biotec.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
                            <h4>Ingeniería en Biotecnología</h4>
                            <p>
                                Ipsum in aspernatur ut possimus sint. Quia omnis est occaecati possimus ea. Quas
                                molestiae perspiciatis occaecati qui rerum. Deleniti quod porro sed quisquam saepe.
                                Numquam mollitia recusandae non ad at et a.
                            </p>
                            <p>
                                Ad vitae recusandae odit possimus. Quaerat cum ipsum corrupti. Odit qui asperiores ea
                                corporis deserunt veritatis quidem expedita perferendis. Qui rerum eligendi ex doloribus
                                quia sit. Porro rerum eum eum.
                            </p>
                        </div>
                    </div>

                    <div class="row about-extra">
                        <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
                            <img src="/assets/ISC.jpg" class="img-fluid" alt="">
                        </div>

                        <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
                            <h4>Ingeniería en sistemas computacionales</h4>
                            <p>
                                Delectus alias ut incidunt delectus nam placeat in consequatur. Sed cupiditate quia ea
                                quis. Voluptas nemo qui aut distinctio. Cumque fugit earum est quam officiis numquam.
                                Ducimus corporis autem at blanditiis beatae incidunt sunt.
                            </p>
                            <p>
                                Voluptas saepe natus quidem blanditiis. Non sunt impedit voluptas mollitia beatae. Qui
                                esse molestias. Laudantium libero nisi vitae debitis. Dolorem cupiditate est perferendis
                                iusto.
                            </p>
                            <p>
                                Eum quia in. Magni quas ipsum a. Quis ex voluptatem inventore sint quia modi. Numquam
                                est aut fuga mollitia exercitationem nam accusantium provident quia.
                            </p>
                        </div>

                    </div>

                    <div class="row about-extra">
                        <div class="col-lg-6 wow fadeInUp">
                            <img src="/assets/energia.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
                            <h4>Ingeniería en energia</h4>
                            <p>
                                Ipsum in aspernatur ut possimus sint. Quia omnis est occaecati possimus ea. Quas
                                molestiae perspiciatis occaecati qui rerum. Deleniti quod porro sed quisquam saepe.
                                Numquam mollitia recusandae non ad at et a.
                            </p>
                            <p>
                                Ad vitae recusandae odit possimus. Quaerat cum ipsum corrupti. Odit qui asperiores ea
                                corporis deserunt veritatis quidem expedita perferendis. Qui rerum eligendi ex doloribus
                                quia sit. Porro rerum eum eum.
                            </p>
                        </div>
                    </div>

                    <div class="row about-extra">
                        <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
                            <img src="/assets/industrial.jpg" class="img-fluid" alt="">
                        </div>

                        <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
                            <h4>Ingeniería industrial</h4>
                            <p>
                                Delectus alias ut incidunt delectus nam placeat in consequatur. Sed cupiditate quia ea
                                quis. Voluptas nemo qui aut distinctio. Cumque fugit earum est quam officiis numquam.
                                Ducimus corporis autem at blanditiis beatae incidunt sunt.
                            </p>
                            <p>
                                Voluptas saepe natus quidem blanditiis. Non sunt impedit voluptas mollitia beatae. Qui
                                esse molestias. Laudantium libero nisi vitae debitis. Dolorem cupiditate est perferendis
                                iusto.
                            </p>
                            <p>
                                Eum quia in. Magni quas ipsum a. Quis ex voluptatem inventore sint quia modi. Numquam
                                est aut fuga mollitia exercitationem nam accusantium provident quia.
                            </p>
                        </div>

                    </div>

                </div>

            </div>
        </section>

        <section id="why-us" class="wow fadeIn">
            <div class="container">
                <header class="section-header">
                    <h3>¿Por qué elegirnos?</h3>
                    <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant
                    vituperatoribus.</p>
                </header>

                <div class="row row-eq-height justify-content-center">

                    <div class="col-lg-4 mb-4">
                        <div class="card wow bounceInUp">
                            <i class="fa fa-diamond"></i>
                            <div class="card-body">
                                <h5 class="card-title">Calidad</h5>
                                <p class="card-text">Deleniti optio et nisi dolorem debitis. Aliquam nobis est
                                temporibus sunt ab inventore officiis aut voluptatibus.</p>
                                <a href="#" class="readmore">Saber mas</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 mb-4">
                        <div class="card wow bounceInUp">
                            <i class="fa fa-language"></i>
                            <div class="card-body">
                                <h5 class="card-title">Materias</h5>
                                <p class="card-text">Voluptates nihil et quis omnis et eaque omnis sint aut. Ducimus
                                dolorum aspernatur.</p>
                                <a href="#" class="readmore">Saber mas</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 mb-4">
                        <div class="card wow bounceInUp">
                            <i class="fa fa-object-group"></i>
                            <div class="card-body">
                                <h5 class="card-title">Programa</h5>
                                <p class="card-text">Autem quod nesciunt eos ea aut amet laboriosam ab. Eos quis porro
                                in non nemo ex. </p>
                                <a href="#" class="readmore">Saber mas</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <section id="testimonials" class="section-bg">
            <div class="container">

                <header class="section-header">
                    <h3>Alumnos</h3>
                </header>

                <div class="row justify-content-center">
                    <div class="col-lg-8">

                        <div class="owl-carousel testimonials-carousel wow fadeInUp">

                            <div class="testimonial-item">
                                <img src="/assets/testimonial-1.jpg" class="testimonial-img" alt="">
                                <h3>Saul Goodman</h3>
                                <h4>Ingeniería Industrial</h4>
                                <p>
                                    Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit
                                    rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam,
                                    risus at semper.
                                </p>
                            </div>

                            <div class="testimonial-item">
                                <img src="/assets/testimonial-2.jpg" class="testimonial-img" alt="">
                                <h3>Sara Wilsson</h3>
                                <h4>Ingeniería en energia</h4>
                                <p>
                                    Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid
                                    cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet
                                    legam anim culpa.
                                </p>
                            </div>

                            <div class="testimonial-item">
                                <img src="/assets/testimonial-3.jpg" class="testimonial-img" alt="">
                                <h3>Jena Karlis</h3>
                                <h4>Ingeniería en sistemas computacionales</h4>
                                <p>
                                    Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem
                                    veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint
                                    minim.
                                </p>
                            </div>

                            <div class="testimonial-item">
                                <img src="/assets/testimonial-4.jpg" class="testimonial-img" alt="">
                                <h3>Matt Brandon</h3>
                                <h4>Ingeniería industrial</h4>
                                <p>
                                    Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim
                                    fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem
                                    dolore labore illum veniam.
                                </p>
                            </div>

                            <div class="testimonial-item">
                                <img src="/assets/testimonial-5.jpg" class="testimonial-img" alt="">
                                <h3>John Larson</h3>
                                <h4>Ingeniería en energia</h4>
                                <p>
                                    Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster
                                    veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam
                                    culpa fore nisi cillum quid.
                                </p>
                            </div>

                        </div>

                    </div>
                </div>


            </div>
        </section>

        <section id="contact">
            <div class="container-fluid">

                <div class="section-header">
                    <h3>Contactanos</h3>
                </div>

                <div class="row wow fadeInUp">

                    <div class="col-lg-6">
                        <div class="map mb-4 mb-lg-0">
                            <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3667.123781362843!2d-102.85244798502895!3d23.20215918486251!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86830ae219d0545d%3A0x43ba6811dc4cb01f!2sPlan%20de%20Pardillo%2C%20Parque%20Industrial%2C%2099059%20Fresnillo%2C%20Zac.!5e0!3m2!1ses!2smx!4v1575518925903!5m2!1ses!2smx"
                            frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-md-5 info">
                                <i class="ion-ios-location-outline"></i>
                                <p>Plan del Pardillo S/N, Parque Industrial,Fresnillo, Zac.</p>
                            </div>
                            <div class="col-md-4 info">
                                <i class="ion-ios-email-outline"></i>
                                <p>upz@upz.com</p>
                            </div>
                            <div class="col-md-3 info">
                                <i class="ion-ios-telephone-outline"></i>
                                <p>5551651</p>
                            </div>
                        </div>

                        <div class="form">
                            <div id="sendmessage">Your message has been sent. Thank you!</div>
                            <div id="errormessage"></div>
                            <form action="" method="post" role="form" class="contactForm">
                                <div class="form-row">
                                    <div class="form-group col-lg-6">
                                        <input type="text" name="name" class="form-control" id="name"
                                        placeholder="Nombre" data-rule="minlen:4"
                                        data-msg="Please enter at least 4 chars" />
                                        <div class="validation"></div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="email" class="form-control" name="email" id="email"
                                        placeholder="Correo" data-rule="email"
                                        data-msg="Please enter a valid email" />
                                        <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="subject" id="subject"
                                    placeholder="Asunto" data-rule="minlen:4"
                                    data-msg="Please enter at least 8 chars of subject" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" data-rule="required"
                                    data-msg="Please write something for us" placeholder="Message"></textarea>
                                    <div class="validation"></div>
                                </div>
                                <div class="text-center"><button type="submit" title="Enviar mensaje">Enviar mensaje</button></div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </section>

    </main>

    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-4 col-md-6 footer-info">
                        <h3>UPZ</h3>
                        <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita
                            valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet
                        proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
                    </div>

                    <div class="col-lg-4 col-md-6 footer-links">
                        <h4>Enlaces de utilidad</h4>
                        <ul>
                            <li class="active"><a href="#intro">Inicio</a></li>
                            <li><a href="#about">Acerca de</a></li>
                            <li><a href="#services">Oferta educativa</a></li>
                            <li><a href="#testimonial">Alumnos</a></li>
                            <li><a href="#contact">Contacto</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-4 col-md-6 footer-contact">
                        <h4>Contact Us</h4>
                        <p>
                            Plan del Pardillo S/N, Parque Industrial,Fresnillo, Zac. 
                            <strong>Telefono:</strong> 55415655<br>
                            <strong>Email:</strong> upz@upz.com<br>
                        </p>

                        <div class="social-links">
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="copyright">
            Copyright © 2019 <strong>Universidad Politécnica de Zacatecas</strong>. All Rights Reserved
        </div>

    </footer>

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <script src="/lib/jquery/jquery.min.js"></script>
    <script src="/lib/jquery/jquery-migrate.min.js"></script>
    <script src="/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/lib/easing/easing.min.js"></script>
    <script src="/lib/mobile-nav/mobile-nav.js"></script>
    <script src="/lib/wow/wow.min.js"></script>
    <script src="/lib/waypoints/waypoints.min.js"></script>
    <script src="/lib/counterup/counterup.min.js"></script>
    <script src="/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="/lib/lightbox/js/lightbox.min.js"></script>
    <script src="/js/main.js"></script>

</body>

</html>