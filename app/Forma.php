<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forma extends Model
{
	protected $fillable = [
		'preguntas','nombre'
	];

	protected $casts = [
		'id' => 'int',
		'preguntas' => 'array'
	];
}
