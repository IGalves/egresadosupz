<?php

namespace App\Http\Controllers;

use App\Encuesta;
use App\Forma;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EncuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $encuesta=Encuesta::where('user_id',Auth::id())->where('forma',$id)->first();
        if(isset($encuesta)){
           return redirect()->route('encuesta.show',$encuesta->id); 
        }

        $v='egresados.'.'encuesta'.$id;
        return view($v);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //
        //dd($request->all());
        //echo json_encode($request->all());

        //dd($request->except(['_token']));
        //echo json_encode($request->except(['_token']));

        $encuesta= Encuesta::create([
            'user_id'=>Auth::id(),
            'forma'=>$id,
            'meta'=>json_encode($request->except(['_token'],JSON_UNESCAPED_UNICODE))
        ]);

        //return redirect()->route('admin.home');
        return redirect()->route('encuesta.create',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Encuesta  $encuesta
     * @return \Illuminate\Http\Response
     */
    public function show(Encuesta $encuesta)
    {
        //

        //dd(json_decode($encuesta->meta,true));
        
        $E=json_decode($encuesta->meta,true);
        $respuestas=array();
        foreach ($E as $key => $value) {
          $respuestas[] = $value==null?"":$value;
        }
        //return $respuestas; 

        $forma=Forma::where('id',$encuesta->forma)->first();
        $P=json_decode($forma->preguntas,true);

        $preguntas=array();
        foreach ($P as $key => $value) {
          $preguntas[] = $value==null?"":$value;
        }
        //return $preguntas;

        //return json_encode($string,JSON_UNESCAPED_UNICODE);

        //dd($encuesta);

        return view('egresados.respuestas',compact('respuestas','preguntas','forma'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Encuesta  $encuesta
     * @return \Illuminate\Http\Response
     */
    public function edit(Encuesta $encuesta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Encuesta  $encuesta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Encuesta $encuesta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Encuesta  $encuesta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Encuesta $encuesta)
    {
        //
    }
}
