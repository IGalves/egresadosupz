<?php

namespace App\Http\Controllers;

use App\Encuesta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(Auth::user()->tipo==='Egresado'){
            $encuesta = Encuesta::where('user_id',Auth::id())->first();
        //dd($encuesta);
            if(!isset($encuesta))
             return redirect()->route('encuesta.create',"1");
     }

     return view('home');

     
 }
}
