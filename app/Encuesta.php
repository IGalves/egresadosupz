<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuesta extends Model
{
	// protected $table = 'encuestas';
	protected $fillable = [
		'meta','user_id','forma'
	];

	protected $casts = [
		'id' => 'int',
		'meta' => 'array'
	];
}
