<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        App\User::create([
            'name' => 'Administrador',
            'email' => 'upzfres@gmail.com',
            'email_verified_at'=>now(),
            'password' => '$2y$10$XXsP3TyiA0z3Y687H310oui1Mwtx8lfxDDT03GgN2lICfKqreaSx.', // 11ad453fc2.a
            'tipo' => 'Administrador',
            'remember_token' => str_random(10)
        ]);
        // factory(App\User::class, 20)->create();

        // Role::create([
        // 	'name'		=> 'Admin',
        // 	'slug'  	=> 'slug',
        // 	'special' 	=> 'all-access'
        // ]);

        // DB::table('role_user')->insert([
        //     'role_id' => '1', 
        //     'user_id' => '1', 
        // ]);
    }
}
