<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes(['verify'=>true]);

// Route::get('/egresados', function () {
// 	return view('egresados.encuesta1');
// })->name('encuesta1');

// Route::get('/egresados2', function () {
// 	return view('egresados.encuesta2');
// })->name('encuesta2');

// Route::get('/tabla', function () {
// 	return view('egresados.contestadas');
// })->name('tabla');

// Route::get('/respuestas', function () {
// 	return view('egresados.respuestas');
// })->name('respuestas');



Route::middleware(['auth'])->group(function () {
	Route::prefix('admin')->group(function () {

		Route::get('/home', 'HomeController@index')->name('admin.home');

		// Route::get('encuesta/{id}', 'EncuestaController@index')->name('encuesta.index');
		Route::get('encuesta/create/{id}', 'EncuestaController@create')->name('encuesta.create');
		Route::post('encuesta/{id}', 'EncuestaController@store')->name('encuesta.store');
		Route::get('encuesta/{encuesta}', 'EncuestaController@show')->name('encuesta.show');

		Route::get('egresados', 'EgresadoController@index')->name('egresados.index');
		// Route::get('users', function () {
  //       // Matches The "/admin/users" URL
		// });

	});
});
